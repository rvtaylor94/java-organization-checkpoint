package com.galvanize;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void returnsCorrectJSONString() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out
        Application.main(new String[] {"s111-08:30am-11:00am", "json"});

        String actual = outContent.toString();
        String expected = "{\n" +
                "  \"type\": \"Suite\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}";
        assertEquals(expected, actual);
    }

    @Test
    public void returnsCorrectHTMLString() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out
        Application.main(new String[] {"r152-08:00am-11:30am", "html"});

        String actual = outContent.toString();
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                "  <dt>Room Number</dt><dd>152</dd>\n" +
                "  <dt>Start Time</dt><dd>08:00am</dd>\n" +
                "  <dt>End Time</dt><dd>11:30am</dd>\n" +
                "</dl>";
        assertEquals(expected, actual);
    }
    @Test
    public void returnsCorrectCSVString() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out
        Application.main(new String[] {"c52-08:00am-11:30am", "csv"});

        String actual = outContent.toString();
        String expected = "type,room number,start time,end time\n" +
                "Classroom,52,08:00am,11:30am";
        assertEquals(expected, actual);
    }


}