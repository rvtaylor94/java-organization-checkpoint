package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.print.Book;

import static org.junit.jupiter.api.Assertions.*;

class JSONFormatterTests {
    JSONFormatter JSONTest;
    @BeforeEach
    void setUp() {
        JSONTest = new JSONFormatter();
    }

    @Test
    void returnsCorrectFormat() {
        String actual = JSONTest.format(new Booking(Booking.RoomType.SUITE, "111", "08:30am",
                "11:00am"));
        String expected = "{\n" +
                "  \"type\": \"Suite\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}";
        assertEquals(expected, actual);
    }
}