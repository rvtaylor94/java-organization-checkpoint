package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HTMLFormatterTests {
    HTMLFormatter HTMLTest;
    @BeforeEach
    void setUp() {
        HTMLTest = new HTMLFormatter();
    }

    @Test
    void returnsCorrectFormat() {
        String actual = HTMLTest.format(new Booking(Booking.RoomType.CONFERENCE_ROOM, "111", "08:30am",
                "11:00am"));
        String expected = "<dl>\n" +
                "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                "  <dt>Room Number</dt><dd>111</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
                "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                "</dl>";
        assertEquals(expected, actual);
    }
}