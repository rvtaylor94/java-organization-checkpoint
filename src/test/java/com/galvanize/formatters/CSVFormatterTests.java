package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVFormatterTests {
    CSVFormatter CSVTest;
    @BeforeEach
    void setUp() {
        CSVTest = new CSVFormatter();
    }

    @Test
    void returnsCorrectFormat() {
        String actual = CSVTest.format(new Booking(Booking.RoomType.AUDITORIUM, "111", "08:30am",
                "11:00am"));
        String expected = "type,room number,start time,end time\n" +
                "Auditorium,111,08:30am,11:00am";
        assertEquals(expected, actual);
    }
}