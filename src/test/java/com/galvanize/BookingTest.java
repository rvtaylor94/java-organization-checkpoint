package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {

    Booking bookingTest;
    @BeforeEach
    void setUp() {
        bookingTest = new Booking(Booking.RoomType.CONFERENCE_ROOM, "14", "8:30", "9:00");
    }

    @Test
    void getRoomType() {
        assertEquals(Booking.RoomType.CONFERENCE_ROOM, bookingTest.getRoomType());
    }

    @Test
    void getRoomNumber() {
        assertEquals("14", bookingTest.getRoomNumber());
    }

    @Test
    void getStartTime() {
        assertEquals("8:30", bookingTest.getStartTime());
    }

    @Test
    void getEndTime() {
        assertEquals("9:00", bookingTest.getEndTime());
    }

    @Test
    void parseHasCorrectType() {
        Booking test = Booking.parse("a14-8:30am-9:00am");

        assertEquals(Booking.RoomType.AUDITORIUM, test.getRoomType());
    }

    @Test
    void parseHasCorrectNumber() {
        Booking test = Booking.parse("a14-8:30am-9:00am");

        assertEquals("14", test.getRoomNumber());
    }

    @Test
    void parseHasCorrectStartTime() {
        Booking test = Booking.parse("a14-8:30am-9:00am");

        assertEquals("8:30am", test.getStartTime());
    }

    @Test
    void parseHasCorrectEndTime() {
        Booking test = Booking.parse("a14-8:30am-9:00am");

        assertEquals("9:00am", test.getEndTime());
    }
}