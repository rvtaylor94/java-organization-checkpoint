package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

public class Application {
    static Formatter getFormatter(String format) {
        switch(format) {
            case "json":
                return new JSONFormatter();
            case "csv":
                return new CSVFormatter();
            case "html":
                return new HTMLFormatter();
            default:
                return null;
        }
    }
    public static void main(String[] args) {
        String bookingCode = args[0];
        String formatCode = args[1];

        Formatter specifiedFormat = getFormatter(formatCode);

        System.out.print(specifiedFormat.format(Booking.parse(bookingCode)));

    }
}