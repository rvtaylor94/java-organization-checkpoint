package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter {

    @Override
    public String format(Booking booking) {
        String formattedRoomType = Booking.getFormattedRoomType(booking.getRoomType());

        return String.format("{%n" +
                "  \"type\": \"%s\",%n" +
                "  \"roomNumber\": %s,%n" +
                "  \"startTime\": \"%s\",%n" +
                "  \"endTime\": \"%s\"%n" +
                "}", formattedRoomType, booking.getRoomNumber(), booking.getStartTime(), booking.getEndTime());
    }
}
