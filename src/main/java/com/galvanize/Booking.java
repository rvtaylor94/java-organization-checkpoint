package com.galvanize;

public class Booking {
    public enum RoomType {
        CONFERENCE_ROOM, SUITE, AUDITORIUM, CLASSROOM
    }
    private RoomType roomType;
    private String roomNumber;
    private String startTime;
    private String endTime;

    public Booking (RoomType type, String roomNumber, String startTime, String endTime) {
        this.roomType = type;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public RoomType getRoomType() {
        return roomType;
    }
    public String getRoomNumber() {
        return roomNumber;
    }
    public String getStartTime() {
        return startTime;
    }
    public String getEndTime() {
        return endTime;
    }

    public static Booking parse(String bookingCode) {
        String[] parsedCode = bookingCode.split("-");
        String roomTypeLetter = String.valueOf(parsedCode[0].charAt(0));
        RoomType roomType = null;
        String roomNumber = parsedCode[0].replace(roomTypeLetter, "");
        String startTime = parsedCode[1];
        String endTime = parsedCode[2];

        switch(roomTypeLetter) {
            case "r":
                roomType = RoomType.CONFERENCE_ROOM;
                break;
            case "s":
                roomType = RoomType.SUITE;
                break;
            case "a":
                roomType = RoomType.AUDITORIUM;
                break;
            case "c":
                roomType = RoomType.CLASSROOM;
                break;
        }

        return new Booking(roomType, roomNumber, startTime, endTime);
    }
    public static String getFormattedRoomType(RoomType roomType) {
        String roomTypeStr = "";
        switch (roomType) {
            case CONFERENCE_ROOM:
                roomTypeStr = "Conference Room";
                break;
            case SUITE:
                roomTypeStr = "Suite";
                break;
            case AUDITORIUM:
                roomTypeStr = "Auditorium";
                break;
            case CLASSROOM:
                roomTypeStr = "Classroom";
                break;
        }
        return roomTypeStr;
    }
}
