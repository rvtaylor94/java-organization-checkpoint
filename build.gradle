group 'galvanize.com'
version '1.0-SNAPSHOT'

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'org.junit.platform:junit-platform-gradle-plugin:1.0.2'
    }
}

apply plugin: 'java'
apply plugin: "jacoco"
apply plugin: "org.junit.platform.gradle.plugin"

sourceCompatibility = 9
targetCompatibility = 9

repositories {
    jcenter()
}

sourceSets {
    assessment {
        java.srcDir file('assessment/java')
        resources.srcDir file('assessment/resources')
    }
}

task assess(type: JavaExec) {
    dependsOn sourceSets.assessment.output.classesDirs
    classpath = sourceSets.assessment.runtimeClasspath

    main = 'org.junit.platform.console.ConsoleLauncher'
    args = ['--scan-class-path',
            sourceSets.assessment.output.getClassesDirs().asPath,
            '--reports-dir', "${buildDir}/test-results/junit-assessment"]
}

jar {
    setProperty("archiveBaseName", '01-java-organization-checkpoint')
    setProperty("archiveVersion", '0.0.1-SNAPSHOT')
    setProperty("archiveFileName", 'booking.jar')

    manifest {
        attributes(
                'Class-Path': configurations.compile.collect { it.getName() }.join(' '),
                'Main-Class': 'com.galvanize.Application'
        )
    }
}

dependencies {
    testCompile 'org.hamcrest:hamcrest-all:1.3'

    testCompile 'org.apiguardian:apiguardian-api:1.0.0',
                'org.junit.jupiter:junit-jupiter-engine:5.0.2',
                'org.junit.jupiter:junit-jupiter-params:5.0.2'

    assessmentCompile group: 'com.google.guava', name: 'guava', version: '23.6-jre'
    assessmentCompile group: 'cglib', name: 'cglib-nodep', version: '2.2'
    assessmentCompile 'org.junit.platform:junit-platform-console:1.0.0'
    assessmentCompile sourceSets.main.output
    assessmentCompile configurations.testCompile
    assessmentRuntime configurations.testRuntime
}

test {
    testLogging {
        exceptionFormat="full"
    }

    jacoco {
        destinationFile file("$buildDir/jacoco/junitPlatformTest.exec")
    }
}

jacoco {
    applyTo junitPlatformTest
}

jacocoTestReport {
    executionData junitPlatformTest
}

jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = 0.9
            }
        }
    }
}

junitPlatformTest.finalizedBy jacocoTestReport
assess.finalizedBy(test)
test.finalizedBy(jacocoTestReport)
jacocoTestReport.finalizedBy(jacocoTestCoverageVerification)

wrapper() {
    gradleVersion = '6.0.1'
    distributionType = Wrapper.DistributionType.ALL
}
